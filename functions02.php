<?php
/**
 * agency_thm functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package agency_thm
 */

if ( ! function_exists( 'agency_thm_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function agency_thm_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on agency_thm, use a find and replace
		 * to change 'agency_thm' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'agency_thm', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-main' => esc_html__( 'Main Menu Top', 'agency_thm' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'agency_thm_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'agency_thm_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function agency_thm_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'agency_thm_content_width', 640 );
}
add_action( 'after_setup_theme', 'agency_thm_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function agency_thm_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'agency_thm' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'agency_thm' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'agency_thm_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function agency_thm_scripts() {
    
    //<!-- Google Fonts -->
    wp_enqueue_style( 'agency_thm-fonts_googleapis','https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' );

    //<!-- Bootstrap CSS File -->
    wp_enqueue_style( 'agency_thm-bootstrap', get_template_directory_uri() . '/assets/lib/bootstrap/css/bootstrap.min.css' );

     //<!-- Libraries CSS Files -->
    wp_enqueue_style( 'agency_thm-font-awesome', get_template_directory_uri() . '/assets/lib/font-awesome/css/font-awesome.min.css' );
	wp_enqueue_style( 'agency_thm-animate', get_template_directory_uri() . '/assets/lib/animate/animate.min.css' );
	wp_enqueue_style( 'agency_thm-ionicons', get_template_directory_uri() . '/assets/lib/ionicons/css/ionicons.min.css' );
	wp_enqueue_style( 'agency_thm-owl_carousel', get_template_directory_uri() . '/assets/lib/owlcarousel/assets/owl.carousel.min.css' );

    //<!-- Main Stylesheet File -->
	wp_enqueue_style( 'agency_thm-style', get_stylesheet_uri() );

//===================================================================
	//<!-- JavaScript Libraries -->
 // <script src="lib/jquery/jquery.min.js"></script>
  
	//<-- jQuery -->
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'agency_thm-jquery-migrate', get_template_directory_uri() . '/assets/lib/jquery/jquery-migrate.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-popper', get_template_directory_uri() . '/assets/lib/popper/popper.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-bootstrap-js', get_template_directory_uri() . '/assets/lib/bootstrap/js/bootstrap.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-easing-js', get_template_directory_uri() . '/assets/lib/easing/easing.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-owlcarousel-js', get_template_directory_uri() . '/assets/lib/owlcarousel/owl.carousel.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-scrollreveal-js', get_template_directory_uri() . '/assets/lib/scrollreveal/scrollreveal.min.js', array('jquery'), '20151215', true );

      //<!-- Contact Form JavaScript File -->
    wp_enqueue_script( 'agency_thm-contactform-js', get_template_directory_uri() . '/assets/contactform/contactform.js', array('jquery'), '20151215', true );

	  //<!-- Template Main Javascript File -->
  	wp_enqueue_script( 'agency_thm-main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'agency_thm-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'agency_thm-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'agency_thm_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



// //=========    menu-main' => 'Main Menu Top'======================


// // add_filter( 'wp_nav_menu_objects', 'filter_function_name_8034', 10, 2 );
// // function filter_function_name_8034( $sorted_items, $args ){
// // 	// Делаем что-либо...
// // 	//echo "string";
    
// //     //var_dump($args);
// //    //print_r($args->theme_location);

// // 	   if ( $args->theme_location === 'menu-main' ) {
// // 			 print_r($args->theme_location);
// //        }

// // 	return $sorted_items;
// // }

// add_filter('wp_nav_menu_objects', 'css_for_nav_parrent',10,2);
// function css_for_nav_parrent( $items,  $args ){
	  
// 	if ( $args->theme_location === 'menu-main' ) {
				
	  

// 		foreach( $items as $index => $item ){
	       
	     


// 			if( __nav_hasSub( $item->ID, $items ) ){

// 				//$item->classes[] ='';
// 			//	print_r($index);
// 				$item->classes[] = ' dropdown';

// 				//$items[$index] = '<p> Hello </p>';
// 				// все элементы поля "classes" меню, будут совмещены и выведены в атрибут class HTML тега <li>
// 			}
// 		}
// 	 }

// 	return $items;
// }
// function __nav_hasSub( $item_id, $items ){
// 	foreach( $items as $item ){

// 		//var_dump($item_id);

// 		if( $item->menu_item_parent && $item->menu_item_parent == $item_id )
// 			return true;
// 	}

// 	return false;
// }

// ///==========




// //===============
// //     add_filter( 'wp_nav_menu_items', 'change_nav_menu_items', 10, 2 );

// // function change_nav_menu_items( $items, $args ) {
// // 	if ( 'menu-main' == $args->theme_location ) {
// // 		$items .= '<li>' . get_search_form( false ) . '</li>';
// // 	}

// // 	return $items;
// // }

// // ==============

// // Изменяет основные параметры меню
// add_filter( 'wp_nav_menu_args', 'filter_wp_menu_args' );
// function filter_wp_menu_args( $args ) {
// 	if ( $args['theme_location'] === 'menu-main' ) {
// 		$args['container']  = 'div';
// 		$args['container_class']  = 'navbar-collapse collapse justify-content-center';
// 		$args['container_id']  = 'navbarDefault';
// 		$args['items_wrap'] = '<ul id="%1$s" class="%2$s">%3$s</ul>'; // Teg - ul -
// 		$args['menu_class'] = 'navbar-nav';  // Teg - ul -
// 		$args['menu_id'] = 'mainNav';  // Teg - ul -
// 	}

// 	return $args;
// }

// // Изменяем атрибут id у тега li
// add_filter( 'nav_menu_item_id', 'filter_menu_item_css_id', 10, 4 );
// function filter_menu_item_css_id( $menu_id, $item, $args, $depth ) {

// 	if ( $args->theme_location === 'menu-main' ) {	
// 			$menu_id = '';



// 	}
// 	return $menu_id;	
// }

// // Изменяем атрибут class у тега li
// // add_filter( 'nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4 );
// // function filter_nav_menu_css_classes( $classes, $item, $args, $depth ) {
// // 	if ( $args->theme_location === 'menu-main' ) {
// // 		//$classes = ['nav-item'];
// // 		 $classes[] .= ' nav-item';
// // 		// $classes[] .= ' '.$depth;
// // 		// $classes[] .= ' '.$item->menu_item_parent;
// // 		// $classes[] .= ' '.$item->ID;


// // 		 // if ( $item->post_parent ) {
// // 		 // 	$classes[] = ' dropdown';
// // 		 // }
       
// //   		// if ( $item->menu_order === 1 ) {
// // 		// 	$classes[] = 'active';
// // 		// }
// // 	}

// // 	return $classes;
// // }

// // // Изменяет класс у вложенного ul
// // add_filter( 'nav_menu_submenu_css_class', 'filter_nav_menu_submenu_css_class', 10, 3 );
// // function filter_nav_menu_submenu_css_class( $classes, $args, $depth ) {
// // 	if ( $args->theme_location === 'menu-main' ) {
// // 		$classes = [
// // 			'menu',
// // 			'menu--dropdown',
// // 			'menu--vertical'
// // 		];
// // 	}

// // 	return $classes;
// // }


// // // Изменяет класс у вложенного ul
// // add_filter( 'nav_menu_submenu_css_class', 'filter_nav_menu_submenu_css_class', 10, 3 );
// // function filter_nav_menu_submenu_css_class( $classes, $args, $depth ) {
// // 	if ( $args->theme_location === 'menu-main' ) {
// // 		$classes = [
// // 			'nav-item',
// // 			'dropdown'
// // 		];
// // 	}

// // 	return $classes;
// // }


// // ДОбавляем классы ссылкам
// add_filter( 'nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4 );
// function filter_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
// 	if ( $args->theme_location === 'menu-main' ) {
// 		$atts['class'] = 'nav-link';

// 		if ( $item->current ) {
// 			$atts['class'] .= ' active';
// 		}
// 	}

// 	return $atts;
// }



// //=======







// // =========


// class my_walker_nav_menu extends Walker_Nav_Menu {

// // public function start_lvl( &$output, $depth = 0, $args = array() ) {
// // 	if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
// // 		$t = '';
// // 		$n = '';
// // 	} else {
// // 		$t = "\t";
// // 		$n = "\n";
// // 	}

// // 	// var_dump($t);


// // 	$indent = str_repeat( $t, $depth );

// //    // var_dump($indent);

// // 	// Default class.
// // 	$classes = array( 'nav-item dropdown' );
// // 	//$classes[] .= ' '.$depth;
// // 	//$classes[] .= ' '.$item->menu_item_parent;
// // 	//$classes[] .= ' '.$item->ID;

// // 	/**
// // 	 * Filters the CSS class(es) applied to a menu list element.
// // 	 *
// // 	 * @since 4.8.0
// // 	 *
// // 	 * @param string[] $classes Array of the CSS classes that are applied to the menu `<ul>` element.
// // 	 * @param stdClass $args    An object of `wp_nav_menu()` arguments.
// // 	 * @param int      $depth   Depth of menu item. Used for padding.
// // 	 */
// // 	$class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
// // 	$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

// // 	// var_dump($class_names);

// // 	$output .= "{$n}{$indent}<li$class_names>{$n}";
// // }


// // public function end_lvl( &$output, $depth = 0, $args = array() ) {
// // 	if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
// // 		$t = '';
// // 		$n = '';
// // 	} else {
// // 		$t = "\t";
// // 		$n = "\n";
// // 	}
// // 	$indent  = str_repeat( $t, $depth );
// // 	$output .= "$indent</li>{$n}";
// // }


// //==============

// public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {


//  // var_dump($item->menu_item_parent);
//  // var_dump($item->current_item_parent); 

//  // var_dump($item->menu_order); 
//  //var_dump($item->ID); 


// 	if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
// 		$t = '';
// 		$n = '';
// 	} else {
// 		$t = "\t";
// 		$n = "\n";
// 	}
// 	$indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

// 	//$classes   = empty( $item->classes ) ? array() : (array) $item->classes;
// 	$classes   = empty( $item->classes ) ? array() : (array) $item->classes;
// 	//$classes[] = 'menu-item-' . $item->ID;
// 	$classes[] = 'nav-item ';




	

	

//      // if($item->menu_item_parent == $item->ID ){

//      // }


// 	/**
// 	 * Filters the arguments for a single nav menu item.
// 	 *
// 	 * @since 4.4.0
// 	 *
// 	 * @param stdClass $args  An object of wp_nav_menu() arguments.
// 	 * @param WP_Post  $item  Menu item data object.
// 	 * @param int      $depth Depth of menu item. Used for padding.
// 	 */

// 	$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

// 	/**
// 	 * Filters the CSS classes applied to a menu item's list item element.
// 	 *
// 	 * @since 3.0.0
// 	 * @since 4.1.0 The `$depth` parameter was added.
// 	 *
// 	 * @param string[] $classes Array of the CSS classes that are applied to the menu item's `<li>` element.
// 	 * @param WP_Post  $item    The current menu item.
// 	 * @param stdClass $args    An object of wp_nav_menu() arguments.
// 	 * @param int      $depth   Depth of menu item. Used for padding.
// 	 */

// 	$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
// 	$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

// 	 /*
// 	 * Filters the ID applied to a menu item's list item element.
// 	 *
// 	 * @since 3.0.1
// 	 * @since 4.1.0 The `$depth` parameter was added.
// 	 *
// 	 * @param string   $menu_id The ID that is applied to the menu item's `<li>` element.
// 	 * @param WP_Post  $item    The current menu item.
// 	 * @param stdClass $args    An object of wp_nav_menu() arguments.
// 	 * @param int      $depth   Depth of menu item. Used for padding.
// 	 */
	 
// 	$id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth );
// 	$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

// 	$output .= $indent . '<li' . $id . $class_names . '>';

// 	$atts                 = array();
// 	$atts['title']        = ! empty( $item->attr_title ) ? $item->attr_title : '';
// 	$atts['target']       = ! empty( $item->target ) ? $item->target : '';
// 	$atts['rel']          = ! empty( $item->xfn ) ? $item->xfn : '';
// 	$atts['href']         = ! empty( $item->url ) ? $item->url : '';
// 	$atts['aria-current'] = $item->current ? 'page' : '';

// 	/**
// 	 * Filters the HTML attributes applied to a menu item's anchor element.
// 	 *
// 	 * @since 3.6.0
// 	 * @since 4.1.0 The `$depth` parameter was added.
// 	 *
// 	 * @param array $atts {
// 	 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
// 	 *
// 	 *     @type string $title        Title attribute.
// 	 *     @type string $target       Target attribute.
// 	 *     @type string $rel          The rel attribute.
// 	 *     @type string $href         The href attribute.
// 	 *     @type string $aria_current The aria-current attribute.
// 	 * }
// 	 * @param WP_Post  $item  The current menu item.
// 	 * @param stdClass $args  An object of wp_nav_menu() arguments.
// 	 * @param int      $depth Depth of menu item. Used for padding.
// 	 */

// 	$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

// 	$attributes = '';
// 	foreach ( $atts as $attr => $value ) {
// 		if ( ! empty( $value ) ) {
// 			$value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
// 			$attributes .= ' ' . $attr . '="' . $value . '"';
// 		}
// 	}

// 	/** This filter is documented in wp-includes/post-template.php */
// 	$title = apply_filters( 'the_title', $item->title, $item->ID );

// 	/**
// 	 * Filters a menu item's title.
// 	 *
// 	 * @since 4.4.0
// 	 *
// 	 * @param string   $title The menu item's title.
// 	 * @param WP_Post  $item  The current menu item.
// 	 * @param stdClass $args  An object of wp_nav_menu() arguments.
// 	 * @param int      $depth Depth of menu item. Used for padding.
// 	 */
// 	$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

// 	$item_output  = $args->before;
// 	$item_output .= '<a' . $attributes . '>';
// 	$item_output .= $args->link_before . $title . $args->link_after;
// 	$item_output .= '</a>';
// 	$item_output .= $args->after;

// 	/**
// 	 * Filters a menu item's starting output.
// 	 *
// 	 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
// 	 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
// 	 * no filter for modifying the opening and closing `<li>` for a menu item.
// 	 *
// 	 * @since 3.0.0
// 	 *
// 	 * @param string   $item_output The menu item's starting HTML output.
// 	 * @param WP_Post  $item        Menu item data object.
// 	 * @param int      $depth       Depth of menu item. Used for padding.
// 	 * @param stdClass $args        An object of wp_nav_menu() arguments.
// 	 */

// 	$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
// }

// // =============
// 	}




	class bootstrap_4_walker_nav_menu extends Walker_Nav_menu {
    
    function start_lvl( &$output, $depth ){ // ul
        $indent = str_repeat("\t",$depth); // indents the outputted HTML
        $submenu = ($depth > 0) ? ' sub-menu' : '';
        $output .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
    }
  
  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){ // li a span
        
    $indent = ( $depth ) ? str_repeat("\t",$depth) : '';
    
    $li_attributes = '';
        $class_names = $value = '';
    
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        
        $classes[] = ($args->walker->has_children) ? 'dropdown' : '';
        $classes[] = ($item->current || $item->current_item_anchestor) ? 'active' : '';
        $classes[] = 'nav-item';
        $classes[] = 'nav-item-' . $item->ID;
        if( $depth && $args->walker->has_children ){
            $classes[] = 'dropdown-menu';
        }
        
        $class_names =  join(' ', apply_filters('nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = ' class="' . esc_attr($class_names) . '"';
        
        $id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
        
        $output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';
        
        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr($item->url) . '"' : '';
        
        $attributes .= ( $args->walker->has_children ) ? ' class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"' : ' class="nav-link"';
        
        $item_output = $args->before;
        $item_output .= ( $depth > 0 ) ? '<a class="dropdown-item"' . $attributes . '>' : '<a' . $attributes . '>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;
        
        $output .= apply_filters ( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    
    }
    
}